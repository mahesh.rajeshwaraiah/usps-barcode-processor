package com.usps.manifest.model;

public class Bcode {

    public long bcode;

    public long getBcode ( ) {
        return bcode;
    }

    public void setBcode (long bcode) {
        this.bcode = bcode;
    }
}
