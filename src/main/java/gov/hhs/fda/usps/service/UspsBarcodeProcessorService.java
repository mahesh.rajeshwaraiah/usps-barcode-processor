package gov.hhs.fda.usps.service;

import com.usps.manifest.model.Bcode;
import gov.hhs.fda.usps.model.Barcode;
import gov.hhs.fda.usps.dao.BarCodeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.Date;

@Service
public class UspsBarcodeProcessorService {

    @Autowired
    BarCodeRepository barCodeRepository;

    private static final Logger logger = LogManager.getLogger( UspsBarcodeProcessorService.class);

    public String getManifestforBarcode(long barcode) {

        logger.info("getManifestforBarcode : barcode " + barcode);
        String manifestInfo = null;

        // Make the Actual Web service call to USPS to get Manifest Info
        RestTemplate restTemplate = new RestTemplate();
        Bcode bcode = new Bcode();
        bcode.setBcode ( barcode );
        manifestInfo = restTemplate.postForObject (  "http://localhost:8084/api/usps/manifest",  bcode, String.class);

        // Store the Request and Response Information along with timestamp in database
        int result = saveManifest(barcode, manifestInfo);
        return manifestInfo;
    }

    public int saveManifest(long bCode, String manifest) {
        logger.info("saveManifest - barcode : " + bCode + " Manifest : " + manifest + " Create Date : " + Instant.now());
<<<<<<< HEAD
        Barcode _barcode = barCodeRepository.save(new Barcode ( bCode, manifest, new Date()));
        if (_barcode != null) {
            return 0;
        } else {
            return 1;
        }

=======
        Barcode barCode = new Barcode();
        barCode.setItemId(String.valueOf(bCode));
        barCode.setCreateDate(new Date());
        barCode.setItemManifest(manifest);
        Barcode _barcode = barCodeRepository.save(barCode);
>>>>>>> 87a67143ddf02037a313261fdc6338ce2999a9d9
    }
}
