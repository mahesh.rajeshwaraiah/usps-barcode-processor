package gov.hhs.fda.usps.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USPS_REQUEST_AUDIT")
public class Barcode {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "REQUEST_ID")
	private long requestId;

	@Column(name = "ITEM_ID")
	private String itemId;

	@Column(name = "ITEM_MANIFEST")
	private String itemManifest;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "STATUS_DETAILS")
	private String statusDetails;

	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "MODIFY_DATE")
	private Date modifyDate;

	@Column(name = "CREATED_BY")
	private long createdBy;

	@Column(name = "MODIFIED_BY")
	private long modifiedBy;

	public Barcode () {

	}

	public Barcode (long requestId, String itemId, String itemManifest, String status,
					String statusDetails, Date createDate, Date modifyDate,
					long createdBy, long modifiedBy) {
		this.requestId = requestId;
		this.itemId = itemId;
		this.itemManifest = itemManifest;
		this.status = status;
		this.statusDetails = statusDetails;
		this.createDate = createDate;
		this.modifyDate = modifyDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemManifest() {
		return itemManifest;
	}

	public void setItemManifest(String itemManifest) {
		this.itemManifest = itemManifest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDetails() {
		return statusDetails;
	}

	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "Barcode [barcode=" + itemId + ", manifest=" + itemManifest + "created_date =" + createDate + "]";
	}

}
