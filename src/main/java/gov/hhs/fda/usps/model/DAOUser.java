package gov.hhs.fda.usps.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USPS_SERVICE_ACCOUNT")
public class DAOUser {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    private long id;
	@Column(name = "USER_NAME")
	private String username;

    @Column(name = "PASSWORD")
	@JsonIgnore
    private String password;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "MODIFY_DATE")
    private Date modifyDate;

    @Column(name = "CREATED_BY")
    private long createdBy;

    @Column(name = "MODIFIED_BY")
    private long modifiedBy;

    @Column(name = "INACTIVE_IND")
    private long inactiveInd;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
	public String toString() {
		return "DAOUser [id=" + id + ", username=" + username + ", password=" + password + "]";
	}

    public long getInactiveInd() {
        return inactiveInd;
    }

    public void setInactiveInd(long inactiveInd) {
        this.inactiveInd = inactiveInd;
    }
}