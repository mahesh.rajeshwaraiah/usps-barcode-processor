package gov.hhs.fda.usps.dao;

import java.util.List;

import gov.hhs.fda.usps.model.Barcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BarCodeRepository extends JpaRepository<Barcode, Long> {
  //List<Barcode> findByPublished(boolean published);

  //List<Barcode> findByTitleContaining(String title);
}
