package gov.hhs.fda.usps.controller;

import gov.hhs.fda.usps.model.Barcode;
import gov.hhs.fda.usps.dao.BarCodeRepository;
import gov.hhs.fda.usps.service.UspsBarcodeProcessorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:8084")
@RestController
@RequestMapping("/api")
public class UspsBarcodeProcessorController {

	private static final Logger logger = LogManager.getLogger(UspsBarcodeProcessorController.class);

	@Autowired
	UspsBarcodeProcessorService uspsBarcodeProcessorService;

	@Autowired
	BarCodeRepository barCodeRepository;

	@PostMapping("/usps/barcode")
	public ResponseEntity<String> getBarcodeById(@RequestBody Barcode barcode) {

		logger.info("Barcode Info : " + barcode.getItemId());

		String barCode = uspsBarcodeProcessorService.getManifestforBarcode (Long.parseLong(barcode.getItemId()));
		if (barCode!= null && barCode.length () > 0 ) {
			return new ResponseEntity<String>(barCode, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
