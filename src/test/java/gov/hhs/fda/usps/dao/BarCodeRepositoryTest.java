package gov.hhs.fda.usps.dao;

import gov.hhs.fda.usps.model.Barcode;
import gov.hhs.fda.usps.service.UspsBarcodeProcessorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.util.Date;

import static org.mockito.Mockito.when;

@ExtendWith ( MockitoExtension.class)
public class BarCodeRepositoryTest {

    @Mock
    private JpaRepository<Barcode, Long> jpaRepository;

    @InjectMocks
    private UspsBarcodeProcessorService uspsBarcodeProcessorService = new UspsBarcodeProcessorService();

    @Test
    public void testSaveBarCode() {

        Barcode b = new Barcode(5600008, "manifest info", new Date ());
        //when(jpaRepository.save ( any() )).thenReturn(0);
        System.out.println("uspsBarcodeProcessorService : " + uspsBarcodeProcessorService);
        assertNotNull(uspsBarcodeProcessorService);
        assertNotNull(jpaRepository);
        //int barcodeResult = uspsBarcodeProcessorService.saveManifest ( 5600008, "manifest xml" );
        //Assertions.assertEquals (barcodeResult, 0);
    }
}
