package gov.hhs.fda.usps.service;

import com.usps.manifest.model.Bcode;
import gov.hhs.fda.usps.dao.BarCodeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith ( MockitoExtension.class)
public class UspsBarcodeProcessorServiceTest {

@Mock
BarCodeRepository barCodeRepository;

@Mock
private RestTemplate restTemplate;

@InjectMocks
private UspsBarcodeProcessorService uspsBarcodeProcessorService = new UspsBarcodeProcessorService();

@Test
public void testGetManifestforBarcode() {

    Bcode bCode = new Bcode ( );
    bCode.setBcode ( 5600008 );
    long barcode = 5600008;
    String manifestXml = "<usps>\n" +
            "    <barcodeno>5600008</barcodeno>\n" +
            "    <package>\n" +
            "        <weight>10 lbs</weight>\n" +
            "        <shipping_type>Standard</shipping_type>\n" +
            "    </package>\n" +
            "    <address>\n" +
            "        <name>John</name>\n" +
            "        <street>1000 Main Street</street>\n" +
            "        <city>Manassas</city>\n" +
            "        <state>VA</state>\n" +
            "        <zip>40023</zip>\n" +
            "        <phone>202-877-9876</phone>\n" +
            "    </address>\n" +
            "</usps>";

    when(barCodeRepository.save ( any() )).thenReturn ( any() );

    String result = uspsBarcodeProcessorService.getManifestforBarcode (barcode );
    Assertions.assertEquals(result, manifestXml);
}

    @Test
    public void testsaveManifest() {

        long barcode = 5600008;
        String manifestXml = "<usps>\n" +
                "    <barcodeno>5600008</barcodeno>\n" +
                "    <package>\n" +
                "        <weight>10 lbs</weight>\n" +
                "        <shipping_type>Standard</shipping_type>\n" +
                "    </package>\n" +
                "    <address>\n" +
                "        <name>John</name>\n" +
                "        <street>1000 Main Street</street>\n" +
                "        <city>Manassas</city>\n" +
                "        <state>VA</state>\n" +
                "        <zip>40023</zip>\n" +
                "        <phone>202-877-9876</phone>\n" +
                "    </address>\n" +
                "</usps>";

        when(barCodeRepository.save ( any() )).thenReturn ( any() );

        int result = uspsBarcodeProcessorService.saveManifest ( barcode, manifestXml );
        Assertions.assertEquals(1, result);

    }
}
