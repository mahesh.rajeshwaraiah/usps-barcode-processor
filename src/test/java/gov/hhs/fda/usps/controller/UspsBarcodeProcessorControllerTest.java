package gov.hhs.fda.usps.controller;

import gov.hhs.fda.usps.dao.BarCodeRepository;
import gov.hhs.fda.usps.service.UspsBarcodeProcessorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith ( MockitoExtension.class)
public class UspsBarcodeProcessorControllerTest {

    @Mock
    UspsBarcodeProcessorService uspsBarcodeProcessorService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    BarCodeRepository barCodeRepository;

    @InjectMocks
    UspsBarcodeProcessorController uspsBarcodeProcessorController;

    @Test
    public void testGetBarcodeById() {

        long barcode = 5600008;
        String manifestXml = "<usps>\n" +
                "    <barcodeno>5600008</barcodeno>\n" +
                "    <package>\n" +
                "        <weight>10 lbs</weight>\n" +
                "        <shipping_type>Standard</shipping_type>\n" +
                "    </package>\n" +
                "    <address>\n" +
                "        <name>John</name>\n" +
                "        <street>1000 Main Street</street>\n" +
                "        <city>Manassas</city>\n" +
                "        <state>VA</state>\n" +
                "        <zip>40023</zip>\n" +
                "        <phone>202-877-9876</phone>\n" +
                "    </address>\n" +
                "</usps>";

        String manifestXmlResult = uspsBarcodeProcessorService.getManifestforBarcode ( barcode );
        Assertions.assertNotEquals(manifestXml, manifestXmlResult);

    }


}
